#!/usr/bin/python
from __future__ import division
__author__ = 'grist'

'''

A script to make patterns for my LED hat.

Using a 7 * 78 pixel raw canvas projected onto
the more parallelogram shaped LED coil.

DEFUNCT: See hat_renderer.py for current development

'''

import sys, re, os

###############################################################################
# Classes
###############################################################################
class Hat:
    ''' A hat. Hard coded values for my hat.
    '''
    def __init__(self):
        # the square canvas
        self.num_leds = 575
        self.num_rows = 7
        self.leds = [] # the led strip
        self.rows = [] # a virtual rectangular representation of the hat. Will be filled by fill_all
        self.fill_all((0,0,0))
        # Can't use a simple multiplier to duplicate the rows are they
        # all end up being references to the same row.

        # the actual canvas with mappings back to the LEDs in the strip
        self.row_map = [
            [i for i in range(86)],
            [i for i in range(86,171)],
            [i for i in range(171,254)],
            [i for i in range(254,336)],
            [i for i in range(336,418)],
            [i for i in range(418,497)],
            [i for i in range(497,575)]
        ]

    def fill_all(self, colour):
        self.leds = [colour] * self.num_leds
        self.rows = [[colour] * 78 for _ in range(self.num_rows)]

    def map(self):
        ''' Map the current contents of rows onto row_map.
        '''
        for y, y_row in enumerate(self.rows):
            for x, val in enumerate(y_row):
                raw_x = int(x/len(y_row) * (len(self.row_map[y])) + 0.5)
                self.leds[self.row_map[y][raw_x]] = val

    def write_out(self, file_name):
        ''' Write out the values in the strip to the file.
        '''
        try:
            fh = open(file_name, 'ab')
        except Exception, e:
            print "Failed to open %s for appending: %s" % (file_name, e)
            return
        # Make a bytearray out of the strip data
        x = [e for i in self.leds for e in i]
        all_bytes = bytearray(x)
        fh.write(all_bytes)
        fh.close()

    def scroll(self, direction, fill=None):
        ''' Scroll the current pattern 1 pixel in the supplied
        direction. Directions can be up, down, left, right, or
        just the first letter of those words. The empty row or
        column created will be filled with fill if it is not None.
        '''
        if direction.startswith('r'):
            for i,row in enumerate(self.rows):
                row = row.pop()
                if fill is not None:
                    row.insert(0, fill[i])
                else:
                    row.insert(0, (0,0,0))
        return

    def make_square(self, bottom_left, top_right, colour, filled = False):
        ''' Make a square. Coords and colour should be tuples.
        '''
        # top row
        for x in range(bottom_left[0], top_right[0]):
            self.rows[top_right[1]][x] = colour
        # left column
        for y in range(bottom_left[1],top_right[1]):
            self.rows[y][bottom_left[0]] = colour
        # bottom row
        for x in range(bottom_left[0], top_right[0]):
            self.rows[bottom_left[1]][x] = colour
        # right column
        for y in range(bottom_left[1],top_right[1]):
            self.rows[y][top_right[0]] = colour
        self.rows[top_right[1]][top_right[0]] = colour

    def ingest_file(self, infile, outfile):
        ''' Ingest a text file and create a pattern
        file from it.
        '''
        colour_match = re.compile("(?P<index>.)\s*=?\s*(?P<r>\d+),\s?(?P<g>\d+),\s?(?P<b>\d+)")
        # Read the colour map
        colours = {}
        line = ''
        while line != '=====':
            m = colour_match.match(line)
            if m:
                ret = m.groupdict()
                colours[ret['index']] = (int(ret['r']), int(ret['g']), int(ret['b']))
            line = infile.readline().rstrip()
        print "Got %d colours" % len(colours)
        # read the rest of the file
        lines_per_pattern = len(self.rows)
        max_line_len = len(self.rows[0])
        line_number = 0
        self.fill_all((0,0,0)) # clean slate
        for line in (l[:max_line_len] for l in infile):
            for pos, char in enumerate(line.rstrip()):
                try:
                    self.rows[lines_per_pattern - line_number - 1][pos] = colours[char]
                except Exception, e:
                    print 'this'
            line_number = (line_number + 1) % lines_per_pattern
            if line_number == 0:
                self.map()
                self.write_out(outfile)
                self.fill_all((0,0,0))

class Sprite:
    ''' A sprite created from a text file.
    '''
    def __init__(self, file_name):
        self.colour_map = {}
        self.frames = []

    def ingest_file(self, file_name):
        ''' Get the sprite definition from a file.
        '''
        colour_match = re.compile("(?P<index>.)\s*=?\s*(?P<r>\d+),\s?(?P<g>\d+),\s?(?P<b>\d+)")
        try:
            infile = open(file_name)
        except Exception, e:
            print "Couldn't open '%s': %s" % (file_name, e)
            return None

        # Read the colour map
        line = ''
        while line != '=====':
            m = colour_match.match(line)
            if m:
                ret = m.groupdict()
                self.colour_map[ret['index']] = (int(ret['r']), int(ret['g']), int(ret['b']))
            line = infile.readline().rstrip()
        print "Got %d colours" % len(self.colour_map)
        # read the rest of the file
        lines_per_pattern = len(self.rows)
        max_line_len = len(self.rows[0])
        line_number = 0
        for line in (l[:max_line_len] for l in infile):
            for pos, char in enumerate(line.rstrip()):
                try:
                    self.rows[lines_per_pattern - line_number - 1][pos] = self.colour_map[char]
                except Exception, e:
                    print "Failed to parse line '%s': %s" % (line, e)
            line_number = (line_number + 1) % lines_per_pattern
            if line_number == 0:
                self.map()
                self.write_out(outfile)
                self.fill_all((0,0,0))






###############################################################################
# Functions
###############################################################################



###############################################################################
# Code
###############################################################################

file_name = 'scroll_play'

if len(sys.argv) > 1:
    file_name = sys.argv[1]
    try:
        fh = open(file_name)
    except Exception,e:
        print "Couldn't open '%s'" % file_name
        sys.exit()

outfile = file_name + '.pattern'
try:
    os.unlink(outfile)
except Exception:
    pass


hat = Hat()

hat.make_square((0,0),(6,6),(8,0,0))
hat.scroll('r')
hat.map()
hat.write_out(file_name)





#hat.ingest_file(fh, outfile)




'''
output_file = 'square.pattern'
hat = Hat()
hat.make_square((0,0),(6,6),(8,8,8))
hat.make_square((7,0),(13,6),(8,0,0))
hat.make_square((14,0),(20,6),(0,0,8))
hat.make_square((21,0),(27,6),(0,8,0))
hat.make_square((28,0),(34,6),(8,0,8))
hat.make_square((35,0),(41,6),(0,8,8))
hat.make_square((42,0),(48,6),(8,8,0))
hat.map()
hat.write_out(output_file)
#'''

