#!/usr/bin/python
from __future__ import division
__author__ = 'grist'
__doc__ = '''

A script to provide an object for rendering patterns on my LED hat.

0.01 - 2017.11.06 - Initial writing. Some code moved from previous version.

'''
__ver__ = 0.01

from led_hat import LED_Hat
import sys, re, os, time

#-------------------------------------------------------------------------------
# Classes
#-------------------------------------------------------------------------------
class WizHat(LED_Hat):
    ''' An APA102 LED strip, communicating over SPI.
    NB: Primitives do not call show() themselves.
    This is by design, to allow chaining of multiple transformations
    without unintended display issues.

    The parent object takes care of talking to the strip and provides
    clear_strip
    set_pixel
    rotate
    show
    '''
    def __init__(self, num_leds, rows, columns, spi_speed=8000000):
        LED_Hat.__init__(self, num_leds, max_speed_hz=spi_speed)
        self.num_leds = num_leds
        self.num_rows = rows
        self.num_columns = columns
        self.led_strip = []
        self.rows = [] # a virtual rectangular representation of the hat. Will be filled by fill_all
        self.fill_all((0,0,0))
        self.show()
        # Obtained empirically. Used for mapping the rectangular rows onto the skewed trapezoid shaped hat.
        # Works ok provided you don't look at the patterns too closely.
        self.row_map = [
            [i for i in range(86)],
            [i for i in range(86,171)],
            [i for i in range(171,254)],
            [i for i in range(254,336)],
            [i for i in range(336,418)],
            [i for i in range(418,497)],
            [i for i in range(497,575)]
        ]

    def fill_all(self, colour):
        self.led_strip = [colour] * self.num_leds
        self.rows = [[colour] * self.num_columns for _ in range(self.num_rows)]
        self.fill(*colour)

    def map(self, push_to_hardware=True):
        ''' Map the current contents of rows onto the strip using row_map.
        '''
        for y, y_row in enumerate(self.rows):
            for x, val in enumerate(y_row):
                raw_x = int(x/len(y_row) * (len(self.row_map[y])) + 0.5)
                self.led_strip[self.row_map[y][raw_x]] = val
        if push_to_hardware:
            for i,colours in enumerate(self.led_strip):
                self.set_pixel(i,*colours)
            #(self.set_pixel(*colours) for colours in self.led_strip)


                #time.sleep(0.1)


    def write_out(self, file_name):
        ''' Write out the values in the strip to the file.
        '''
        try:
            fh = open(file_name, 'ab')
        except Exception, e:
            print "Failed to open %s for appending: %s" % (file_name, e)
            return
        # Make a bytearray out of the strip data
        x = [e for i in self.led_strip for e in i]
        all_bytes = bytearray(x)
        fh.write(all_bytes)
        fh.close()

    def make_vertical_line(self, column, colour):
        ''' Make a vertical line.
        '''
        for row in self.rows:
            row[column] = colour

    def make_horizontal_line(self, row, colour):
        ''' Make a horizontal line.
        '''
        for i in range(len(self.rows[row])):
            self.rows[row][i] = colour

    def make_square(self, bottom_left, top_right, colour, filled = False):
        ''' Make a square. Coords and colour should be tuples.
        '''
        # top row
        for x in range(bottom_left[0], top_right[0]):
            self.rows[top_right[1]][x] = colour
        # left column
        for y in range(bottom_left[1],top_right[1]):
            self.rows[y][bottom_left[0]] = colour
        # bottom row
        for x in range(bottom_left[0], top_right[0]):
            self.rows[bottom_left[1]][x] = colour
        # right column
        for y in range(bottom_left[1],top_right[1]):
            self.rows[y][top_right[0]] = colour
        self.rows[top_right[1]][top_right[0]] = colour

    def fade_all_rows(self, percentage):
        ''' Fade all the LEDs. Well, the ones represented in self.rows, ie the square
        virtual canvass.
        '''
        for r, row in enumerate(self.rows):
            for c, col in enumerate(row):
                self.rows[r][c] = tuple(int(i * (100-percentage)/100) for i in col)

    def kitt(self, r, g, b):
        ''' Kitt using the full height of the hat.
        '''
        fade_percent = 10
        while 1:
            for col in range(self.num_columns):
                self.make_vertical_line(col, (r,g,b))
                self.map()
                self.fade_all_rows(fade_percent)
                self.map()
                self.show()
            for col in range(self.num_columns-1,-1,-1):
                self.make_vertical_line(col, (r,g,b))
                self.map()
                self.fade_all_rows(fade_percent)
                self.map()
                self.show()

    def make_kitt_pattern(self, repetitions, file_name):
        ''' Write the kitt pattern out to a binary file for later
        playback.
        '''
        r = 32
        g = b = 0
        fade_percent = 10
        while repetitions > 0:
            for col in range(self.num_columns):
                self.make_vertical_line(col, (r,g,b))
                self.map()
                self.fade_all_rows(fade_percent)
                self.map()
                self.write_out(file_name)
            for col in range(self.num_columns-1,-1,-1):
                self.make_vertical_line(col, (r,g,b))
                self.map()
                self.fade_all_rows(fade_percent)
                self.map()
                self.write_out(file_name)
            repetitions -= 1


class Sprite:
    ''' A sprite created from a text file.
    '''
    def __init__(self, file_name):
        self.colour_map = {}
        self.frames = []

    def ingest_file(self, file_name):
        ''' Get the sprite definition from a file.
        '''
        colour_match = re.compile("(?P<index>.)\s*=?\s*(?P<r>\d+),\s?(?P<g>\d+),\s?(?P<b>\d+)")
        try:
            infile = open(file_name)
        except Exception, e:
            print "Couldn't open '%s': %s" % (file_name, e)
            return None

        # Read the colour map
        line = ''
        while line != '=====':
            m = colour_match.match(line)
            if m:
                ret = m.groupdict()
                self.colour_map[ret['index']] = (int(ret['r']), int(ret['g']), int(ret['b']))
            line = infile.readline().rstrip()
        print "Got %d colours" % len(self.colour_map)
        # read the rest of the file
        lines_per_pattern = len(self.rows)
        max_line_len = len(self.rows[0])
        line_number = 0
        for line in (l[:max_line_len] for l in infile):
            for pos, char in enumerate(line.rstrip()):
                try:
                    self.rows[lines_per_pattern - line_number - 1][pos] = self.colour_map[char]
                except Exception, e:
                    print "Failed to parse line '%s': %s" % (line, e)
            line_number = (line_number + 1) % lines_per_pattern
            if line_number == 0:
                self.map()
                #self.write_out(outfile)
                self.fill_all((0,0,0))


#---------------------------------------------------
#  Functions
#---------------------------------------------------

def get_range_colour(distance, max_range):
    ''' Return an RGB triplet for a given distance based on a sliding
    colour scale:
    black - purple - blue - green - red - yellow - white
    '''
    colours = [
        (0,0,0), # black
        (255,0,255), # purple
        (0,0,255), # blue
        (0,255,0), # green
        (255,0,0), # red
        (255,255,0), # yellow
        (255,255,255), # white
    ]
    # simple linear transform for now
    range = distance/max_range
    # figure out which colour division this lies in
    division = range * (len(colours)-1)
    outer_bound = colours[int(division)]
    inner_bound = colours[int(division + 1)]
    # how far into the band are we
    diff = division - int(division)
    return (
        int(outer_bound[0] + (diff * (inner_bound[0] - outer_bound[0]))),
        int(outer_bound[1] + (diff * (inner_bound[1] - outer_bound[1]))),
        int(outer_bound[2] + (diff * (inner_bound[2] - outer_bound[2]))),
    )

#---------------------------------------------------
#  Code starts here
#---------------------------------------------------


num_leds = 575
rows = 7
cols = 78

# getting some interference near the end of the stip with 8MHz
spi_speed = 6250000  #Hz

hat = WizHat(num_leds, rows, cols, spi_speed=spi_speed)

if len(sys.argv) > 1:
    args = sys.argv[1:]
    while len(args) > 0:
        arg = args.pop(0)
        if arg == '-f':
            file_name = args.pop(0)
            print "Playing %s as binary" % file_name
            while True:
                hat.play_binary_file(file_name)
        elif arg == '-kitt':
            hat.kitt(32,0,0)
        elif arg == '-c':
            file_name = args.pop(0)
            print "Compiling %s" % file_name
            hat.play_pattern(file_name, "%s.%s" % (file_name,'pattern'))
        elif arg == '-p':
            file_name = args.pop(0)
            print "Playing %s as text" % (file_name)
            while True:
                hat.play_pattern(file_name)
#hat.kitt(8,0,0)
