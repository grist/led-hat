__author__ = 'grist'

'''

A script to generate some ascii sprites.

'''


sprites = {
    'invader_one': [
'''\
  wwww
 wwwwww
ww ww ww
wwwwwwww
  w  w
 w ww w
w w  w w
'''.splitlines(),

'''\
  wwww
 wwwwww
ww ww ww
wwwwwwww
  w  w
 w ww w
 w    w
'''.splitlines(),
],
    'invader_two': [
'''\
w  w   w  w
w wwwwwww w
www www www
wwwwwwwwwww
 wwwwwwwww
  w     w
 w       w
'''.splitlines(),
'''\
   w   w
  wwwwwww
www www www
wwwwwwwwwww
wwwwwwwwwww
w w     w w
   w   w
'''.splitlines(),
],
    'ghost_blue': [
'''\
  bbbbb
 bbbbbbb
bb  b  bb
bb  b  bb
bbbbbbbbb
bbbbbbbbb
b b b b b
'''.splitlines(),
'''\
  bbbbb
 bbbbbbb
bb  b  bb
bb  b  bb
bbbbbbbbb
bbbbbbbbb
b  b b  b
'''.splitlines(),
],
    'pacman_chasing_blue_ghost': [
'''\
   yyy           bbbbb
  yyyyy         bbbbbbb
 yyyyyyy       bb  b  bb
yyyyyyyyy      bb  b  bb
yyyyyyyyy      bbbbbbbbb
 yyyyyyy       bbbbbbbbb
  yyyyy        b b b b b
'''.splitlines(),
'''\
   yyy           bbbbb
  yyyyy         bbbbbbb
 yyyyyyy       bb  b  bb
yyyyyyyyy      bb  b  bb
yyyy           bbbbbbbbb
 yyyyyyy       bbbbbbbbb
  yyyyy        b  b b  b
'''.splitlines(),
'''\
   yyy           bbbbb
  yyyyy         bbbbbbb
 yyyyyyy       bb  b  bb
yyyyy          bb  b  bb
yyyy           bbbbbbbbb
 yyyyyyy       bbbbbbbbb
  yyyyy        b b b b b
'''.splitlines(),
'''\
   yyy           bbbbb
  yyyyy         bbbbbbb
 yyyyy         bb  b  bb
yyyyy          bb  b  bb
yyyy           bbbbbbbbb
 yyyy          bbbbbbbbb
  yyyyy        b  b b  b
'''.splitlines(),
'''\
   yyy           bbbbb
  yyyyy         bbbbbbb
 yyyyy         bb  b  bb
yyyyy          bb  b  bb
yyyy           bbbbbbbbb
 yyyy          bbbbbbbbb
  yyyyy        b b b b b
'''.splitlines(),
'''\
   yyy           bbbbb
  yyyyy         bbbbbbb
 yyyyyyy       bb  b  bb
yyyyy          bb  b  bb
yyyy           bbbbbbbbb
 yyyyyyy       bbbbbbbbb
  yyyyy        b  b b  b
'''.splitlines(),
'''\
   yyy           bbbbb
  yyyyy         bbbbbbb
 yyyyyyy       bb  b  bb
yyyyyyyyy      bb  b  bb
yyyy           bbbbbbbbb
 yyyyyyy       bbbbbbbbb
  yyyyy        b b b b b
'''.splitlines(),
'''\
   yyy           bbbbb
  yyyyy         bbbbbbb
 yyyyyyy       bb  b  bb
yyyyyyyyy      bb  b  bb
yyyyyyyyy      bbbbbbbbb
 yyyyyyy       bbbbbbbbb
  yyyyy        b  b b  b
'''.splitlines(),
],
    'pacman': [
'''\
   yyy
  yyyyy
 yyyyyyy
yyyyyyyyy
yyyyyyyyy
 yyyyyyy
  yyyyy
'''.splitlines(),
'''\
   yyy
  yyyyy
 yyyyyyy
yyyyyyyyy
yyyy
 yyyyyyy
  yyyyy
'''.splitlines(),
'''\
   yyy
  yyyyy
 yyyyyyy
yyyyy
yyyy
 yyyyyyy
  yyyyy
'''.splitlines(),
'''\
   yyy
  yyyyy
 yyyyy
yyyyy
yyyy
 yyyy
  yyyyy
'''.splitlines(),
'''\
   yyy
  yyyyy
 yyyyy
yyyyy
yyyy
 yyyy
  yyyyy
'''.splitlines(),
'''\
   yyy
  yyyyy
 yyyyyyy
yyyyy
yyyy
 yyyyyyy
  yyyyy
'''.splitlines(),
'''\
   yyy
  yyyyy
 yyyyyyy
yyyyyyyyy
yyyy
 yyyyyyy
  yyyyy
'''.splitlines(),
'''\
   yyy
  yyyyy
 yyyyyyy
yyyyyyyyy
yyyyyyyyy
 yyyyyyy
  yyyyy
'''.splitlines(),
]}

sprite = 'invader_one'

outfile = '%s.sprite' % sprite
fh = open(outfile,'w')

fh.write(
'''# A sprite. First up, the colour definitions. Terminated with =====
# lines that don't match the regex are ignored. The first definition binds the
# space character to a blank LED. Any valid ASCII character can be defined as a
# colour.

  = 0,0,0
r = 8,0,0
g = 0,8,0
b = 0,0,8
y = 8,8,0
p = 8,0,8
w = 8,8,8

# Patterns are 7 lines long and 78 characters wide. 78 characters do not need to be supplied,
# anything beyond the last character is assumed to be blank. Anything over 78 characters will
# be discarded. Everything beyond this point is read as data.
=====
'''
)

reps = 3
i = 0
while i < 78:
    for invader in sprites[sprite]:
        for _ in range(reps):
            for line in invader:
                fstring = "%s%s\n" % (' ' * i, line)
                fh.write(fstring)
        i += 1



