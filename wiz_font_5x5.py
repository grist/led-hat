__doc__ = '''

A basic text font

'''


letters = {
' ': '''\




''',

'A': '''\
 XXX
X   X
XXXXX
X   X
X   X''',

'B': '''\
XXX
X  X
XXX
X  X
XXX''',

'C': '''\
 XXX
X   X
X
X   X
 XXX''',

'D': '''\
XXXX
X   X
X   X
X   X
XXXX''',

'E': '''\
XXXXX
X
XXX
X
XXXXX''',

'F': '''\
XXXXX
X
XXX
X
X''',

'G': '''\
 XXX
X   X
X
X  XX
 XXX''',

'H': '''\
X   X
X   X
XXXXX
X   X
X   X''',

'I': '''\
XXXXX
  X
  X
  X
XXXXX''',

'J': '''\
    X
    X
    X
X   X
 XXX''',

'K': '''\
X   X
X  X
XX
X  X
X   X''',

'L': '''\
X
X
X
X
XXXX''',

'M': '''\
 X   X
X X X X
X  X  X
X     X
X     X''',

'N': '''\
X   X
XX  X
X X X
X  XX
X   X''',

'O': '''\
 XXX
X   X
X   X
X   X
 XXX''',

'P': '''\
XXXX
X   X
XXXX
X
X''',

'Q': '''\
 XXX
X   X
X   X
X  XX
 XXX''',

'R': '''\
XXXX
X   X
XXXX
X   X
X   X''',

'S': '''\
 XXX
 X  X
  X
X  X
 XX''',

'T': '''\
XXXXX
  X
  X
  X
  X''',

'U': '''\
X   X
X   X
X   X
X   X
 XXX''',

'V': '''\
X   X
X   X
X   X
 X X
  X''',

'W': '''\
X     X
X     X
X  X  X
X X X X
 X   X''',

'X': '''\
X   X
 X X
  X
 X X
X   X''',

'Y': '''\
X   X
 X X
  X
  X
  X''',

'Z': '''\
XXXXX
   X
  X
 X
XXXXX'''

}

