__author__ = 'grist'
'''

A script to take a sentence or a file and
create a text pattern file for led_hat.py
to play back. Or pattern_maker.py to turn
it into a binary file.

0.01    2020-07-31  Initial writing.

'''

import os, sys
from wiz_font_5x5 import letters

#--------------------------------------------
#  Classes
#--------------------------------------------
class Marquee:
    ''' An object to draw and write out frames.
    '''
    def __init__(self, file_name, sentence=None, colours=None):
        self.sentence = sentence
        self.file_name = file_name
        self.outf = self.open_file()
        self.frame = [] # 7 x 78
        self.font = letters
        self.colours = colours

    def open_file(self):
        ''' Open the file for writing and attach
        the handle to the object
        '''
        try:
            fh = open(self.file_name,'w')
        except Exception, e:
            print "Couldn't open %s for writing: %s" % (self.file_name, e)
            exit()

        return fh

    def write_header(self):
        ''' Write out the header.
        '''
        for k,v in self.colours.iteritems():
            self.outf.write("%s = %s" % (k, ','.join(map(str,v))))

        self.outf.write("\n=====\n")

    def write_frame(self):
        ''' Append the current frame to the output.
        '''
        for line in self.frame:
            self.outf.write(''.join(line) + "\n")

    def advance_frame(self, wrap=False):
        ''' Scroll the frame 1 pixel clockwise, If wrap
        is true pixels will map from the end back to the
        start, If not they will just fall off.
        '''
        for line in self.frame:
            if wrap is True:
                # there should be a way to do this in one line
                tmp = line.pop(0)
                line.append(tmp)
            else:
                line.pop(0)
                line.append(' ')

    def make_sentence(self, font_colour, wrap=False):
        ''' Assumes we have a sentence, a font, a colour
        and an open file handle.
        '''
        # make a blank frame to start with, 7x78
        self.frame = []
        blank_line = []
        blank_line.extend(' ' * 78)
        for _ in range(7):
            # append a copy of the line, otherwise they're all the same line
            self.frame.append(blank_line[:])

        for letter in self.sentence:
            letter_pattern = self.font[letter]
            # find the widest point, Will need to pad some lines out to this
            lines = letter_pattern.split("\n")
            max_width = max(map(len,lines))
            for idx in range(max_width):
                for x in range(5):
                    try:
                        self.frame[x+1][-1] = lines[x][idx]
                    except IndexError:
                        self.frame[x+1][-1] = ' '

                self.write_frame()
                self.advance_frame(wrap)
            # put some space between the letters
            self.advance_frame(wrap)
            self.write_frame()
            self.advance_frame(wrap)
            self.write_frame()



#--------------------------------------------
#  Functions
#--------------------------------------------


#--------------------------------------------
#  Code starts here
#--------------------------------------------

fontfile = 'wizfont.fnt'
outfile = 'catathon.txt'
sentence = "catathon               ".upper()
colours = {
    'X': (8,8,8)
}

marquee = Marquee(outfile, sentence, colours)
marquee.write_header()
for x in range(10):
    marquee.make_sentence('w', wrap=True)

marquee.outf.close()
print "Fin."









'''
marquee.frame = [
    [' ',' ',' ',' ','w'],
    [' ',' ',' ',' ','w'],
    [' ',' ',' ',' ','w'],
    [' ',' ',' ',' ','w'],
    [' ',' ',' ',' ','w'],
    [' ',' ',' ',' ','w'],
    [' ',' ',' ',' ','w'],
]

for x in range(12):
    marquee.write_frame()
    marquee.advance_frame(wrap=True)
'''