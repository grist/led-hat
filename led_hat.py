#!/usr/bin/python
from __future__ import division
__author__ = 'grist'
__doc__ = '''

A script to play with an APA102 LED strip on SPI0 of a
Raspberry Pi.

0.02 - 2017.11.06 - Removing anything not hardware related, so this is just an
    object with interfaces to the strip itself and a few basic helper functions.
    Anything more complicated moved to other scripts.

'''
__ver__ = 0.02

from apa102 import APA102
from time import sleep
import sys, re

#-------------------------------------------------------------------------------
# Classes
#-------------------------------------------------------------------------------
class LED_Hat(APA102):
    ''' An APA102 LED strip, communicating over SPI.
    NB: Primitives do not call show() themselves.
    This is by design, to allow chaining of multiple transformations
    without unintended display issues.

    The parent object takes care of talking to the strip and provides
    clear_strip
    set_pixel
    rotate
    show
    '''
    def __init__(self, num_leds, max_speed_hz=6250000):
        APA102.__init__(self, num_leds, max_speed_hz=max_speed_hz)

    # Primitives
    def fill(self, r,g,b):
        ''' Fill the strip with the RGB colour
        '''
        for i in range(self.num_led):
            self.set_pixel(i, r, g, b)

    def get_pixel(self,led_num):
        ''' Return an RGB tuple of the selected pixel
        Can be overidden to use the virtual version if
        desired.
        '''
        start_index = 4 * led_num
        return(
            self.leds[start_index + self.rgb[0]],
            self.leds[start_index + self.rgb[1]],
            self.leds[start_index + self.rgb[2]],
        )

    def fade_all(self, percentage):
        ''' Fade all of the LEDs by the supplied amount. RGB values
        are faded individually.
        '''
        for i in range(0, self.num_led * 4, 4):
            self.leds[i+1] = int(self.leds[i+1] * (100-percentage)/100)
            self.leds[i+2] = int(self.leds[i+2] * (100-percentage)/100)
            self.leds[i+3] = int(self.leds[i+3] * (100-percentage)/100)

    def strobe(self, r, g, b):
        ''' Strobe the colour.
        '''
        # microseconds
        on_time =  10
        off_time = 500
        while(1):
            self.fill(r,g,b)
            self.show()
            sleep(on_time/1000)
            self.fill(0,0,0)
            self.show()
            sleep(off_time/1000)

    def play_pattern(self, file_name, dest='hat'):
        ''' Ingest a text file and create a pattern
        file from it. If dest is not 'hat', a file
        will be created containing the compiled pattern
        '''
        infile = open(file_name)
        outfile = "%s%s" % (file_name,'.pattern')
        colour_match = re.compile("(?P<index>.)\s*=?\s*(?P<r>\d+),\s?(?P<g>\d+),\s?(?P<b>\d+)")
        # Read the colour map
        colours = {}
        line = ''
        while line != '=====':
            m = colour_match.match(line)
            if m:
                ret = m.groupdict()
                colours[ret['index']] = (int(ret['r']), int(ret['g']), int(ret['b']))
            line = infile.readline().rstrip()
        #print "Got %d colours" % len(colours)
        # read the rest of the file
        lines_per_pattern = len(self.rows)
        max_line_len = len(self.rows[0])
        line_number = 0
        self.fill_all((0,0,0)) # clean slate
        for line in (l[:max_line_len] for l in infile):
            for pos, char in enumerate(line.rstrip()):
                try:
                    self.rows[lines_per_pattern - line_number - 1][pos] = colours[char]
                except KeyError, e:
                    pass #rint "Error: '%s'" % (str(e))
            line_number = (line_number + 1) % lines_per_pattern
            if line_number == 0:
                self.map()
                if dest == 'hat':
                    self.show()
                else:
                    self.write_out(outfile)
                self.fill_all((0,0,0))

    def write_out(self, file_name):
        ''' Write out the values in the strip to the file.
        '''
        try:
            fh = open(file_name, 'ab')
        except Exception, e:
            print "Failed to open %s for appending: %s" % (file_name, e)
            return
        # Make a bytearray out of the strip data
        x = [e for i in self.led_strip for e in i]
        all_bytes = bytearray(x)
        fh.write(all_bytes)
        fh.close()

    def play_binary_file(self, file_name, repetitions=1):
        ''' Read a binary file as raw RGB colours and play them out on
        the hat.
        '''
        try:
            fh = open(file_name,'rb')
        except Exception, e:
            print "Failed to open %s: %s" % (file_name, e)
            return
        num_bytes = self.num_led * 3
        for _ in range(repetitions):
            bytes = bytearray(fh.read(num_bytes))
            while(bytes):
                for i in range(self.num_led):
                    try:
                        r,g,b = bytes[i*3], bytes[i*3+1], bytes[i*3+2]
                    except IndexError:
                        break # ran out of bytes

                    self.set_pixel(i,r,g,b)

                self.show()
                bytes = bytearray(fh.read(num_bytes))
#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------
def show_help():
    print '''

    led_hat.py by grist.

    -p to play a text pattern file
    -c to compile a text pattern file to binary
    -f to play a binary file
    '''


#-------------------------------------------------------------------------------
# Code starts here
#-------------------------------------------------------------------------------


if __name__ == "__main__":
    hat = LED_Hat(575)
    args = sys.argv[1:]
    while len(args) > 1:
        arg = args.pop(0)
        if arg == '-f':
            file_name = args.pop(0)
            print "Playing binary file: %s" % file_name
            hat.play_binary_file(file_name)
        elif arg == '-c':
            file_name = args.pop(0)
            print "Compiling pattern file: %s" % file_name
            hat.play_pattern(file_name, dest='file')
        elif arg == '-p':
            file_name = args.pop(0)
            print "Playing pattern file: %s" % file_name
            hat.play_pattern(file_name)
        else:
            print "%d args" % len(sys.argv)
            show_help()
            sys.exit()













