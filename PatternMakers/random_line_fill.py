#!/usr/bin/env python
from __future__ import division
__author__ = 'grist'
__doc__ = '''

A script to generate a bunch of random lines.

'''
__ver__ = 0.01

import WizCanvas
import random, math


#-------------------------------------------------------------------------------  
# Classes
#-------------------------------------------------------------------------------
class Renderer:
    def __init__(self,x,y, colours):
        self.x_max = x
        self.y_max = y
        self.frame = WizCanvas.WizCanvas(x,y)
        self.frame.fill_all(' ')
        self.header = self.make_header(colours)

    def make_header(self, colours):
        ''' Generate a header with the number of colours defined by the bit depth in
        an RGB colour space.
        '''
        header = ''
        for char,(r,g,b) in colours.iteritems():
            header += "%s = %d,%d,%d\n" % (char,r,g,b)
        header += "=====\n"
        return header

    def add_line(self, (x1,y1),(x2,y2), colour ):
        line = self.frame.line((x1,y1),(x2,y2))
        for x,y in line:
            self.frame.frame[x][y] = colour


#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------
def make_random_lines(file_name, colours, num_lines):
    ''' Make a file with random lines drawn all over the hat.
    '''

    random.seed()

    renderer = Renderer(x_max, y_max, colours)

    with open(file_name, 'w') as outfile:
        outfile.write(renderer.header)
        for i in range(num_lines):
            x1 = random.randrange(x_max)
            y1 = random.randrange(y_max)
            x2 = random.randrange(x_max)
            y2 = random.randrange(y_max)
            colour = random.choice(colours.keys())
            renderer.add_line((x1,y1),(x2,y2),colour)
            # need to transform this from x,y to y,x
            for y in range(len(renderer.frame.frame[0])):
                for x in range(len(renderer.frame.frame)):
                    outfile.write(renderer.frame.frame[x][y])
                outfile.write("\n")

    outfile.close()

def random_horizontal_lines(file_name, num_lines):

    colours = WizCanvas.generate_colours(255,8)
    renderer = Renderer(x_max, y_max, colours)

    with open(file_name,'w') as outf:
        outf.write(renderer.header)
        for i in range(num_lines):
            colour = random.choice(colours.keys())
            row = random.randrange(len(renderer.frame.frame[0]))
            renderer.frame.fill_row(row,colour)
            # need to transform this from x,y to y,x
            for y in range(len(renderer.frame.frame[0])):
                for x in range(len(renderer.frame.frame)):
                    outf.write(renderer.frame.frame[x][y])
                outf.write("\n")

    outf.close()

def random_vertical_lines(file_name, num_lines):

    colours = WizCanvas.generate_colours(255,8)
    renderer = Renderer(x_max, y_max, colours)

    with open(file_name,'w') as outf:
        outf.write(renderer.header)
        for i in range(num_lines):
            colour = random.choice(colours.keys())
            col = random.randrange(len(renderer.frame.frame))
            renderer.frame.fill_column(col,colour)
            # need to transform this from x,y to y,x
            for y in range(len(renderer.frame.frame[0])):
                for x in range(len(renderer.frame.frame)):
                    outf.write(renderer.frame.frame[x][y])
                outf.write("\n")

    outf.close()


def three_phase_sine_wave(file_name):
    ''' Outputs a raw binary file
    '''
    outf = open(file_name,'wb')
    colours = bytearray()
    for x in range(360):
        for _ in range(575): # raw number of leds
            colours.append(int(math.sin(math.radians(x)) * 127) + 127)
            colours.append(int(math.sin(math.radians(x + 120)) * 127) + 127)
            colours.append(int(math.sin(math.radians(x + 240)) * 127) + 127)
    outf.write(colours)
    outf.close()



#-------------------------------------------------------------------------------
# Code starts here
#-------------------------------------------------------------------------------

x_max = 78
y_max = 7
file_name = '1000_random_vertical_lines.txt'
lines = 1000

std_colours = {
    ' ': (0,0,0),
    'w': (8,8,8),
    'r': (8,0,0),
    'g': (0,8,0),
    'b': (0,0,8),
    'y': (8,8,0),
    'p': (8,0,8),
}

three_phase_sine_wave('three_phase_sine_wave')
#random_vertical_lines(file_name, lines)





