#!/usr/bin/env python
from __future__ import division
__author__ = 'grist'
__doc__ = '''

A script to provide a drawing space and some drawing primitives.

'''
__ver__ = 0.01

import random

#-------------------------------------------------------------------------------  
# Classes
#-------------------------------------------------------------------------------
class WizCanvas:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        # Create an empty frame ready for populating.
        self.frame = [[None] * self.height for _ in range(self.width)]

#    def __setattr__(self, key, value):
#        self.frame[key] = value

#    def __getattr__(self, item):
#        return self.frame[item]

    def fill_all(self, data):
        ''' Fill every cell with whatever's provided
        '''
        self.frame = [[data] * self.height for _ in range(self.width)]

    def fill_row(self, row_num, data):
        ''' Fill the row with the data
        '''
        for col in self.frame:
            col[row_num] = data

    def fill_column(self, col_num, data):
        ''' Fill the column with the data.
        '''
        self.frame[col_num] = [data] * self.height

    def line(self, (x1,y1),(x2,y2)):
        ''' Generate the coordinates for points that lie on the line. Unbounded.
        '''

        # variables used often by the algorithm
        delta_x = x2 - x1
        delta_y = y2 - y1
    
        # first deal with the 4 straight line cases cos they're easy and don't need
        # the complexity of the main algorithm.
        if delta_x == 0 and delta_y == 0: # it's a point
            yield(x1,y1)
        elif delta_x == 0: # North/South
            for y in range(y1,y2,int((y2-y1)/abs(y2-y1))):
                yield(x1,y)
            yield(x2,y2)
        elif delta_y == 0:  # East/West
            for x in range(x1,x2,int((x2-x1)/abs(x2-x1))):
                yield(x,y1)
            yield(x2,y2)
        # now the diagonals
        elif abs(delta_x) > abs(delta_y): # a mostly horizontal line
            m = delta_y/delta_x
            b = y1 - m * x1
            for x in range(x1,x2+1,int((x2-x1)/abs(x2-x1))):
                y = int((x * m + b) + 0.5)
                yield(x,y)
        else: # a mostly vertical line
            m = delta_x/delta_y
            b = x1 - m * y1
            for y in range(y1,y2+1,int((y2-y1)/abs(y2-y1))):
                x = int((y * m + b) + 0.5)
                yield(x,y)


#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------
def generate_colours(num_colours, bit_depth):
    ''' Generate a random colour pallette with the given bit_depth
    '''
    # Empty space is reserved as blank so we always have one and know what it is.
    colours = {' ': (0,0,0)}
    for i in range(num_colours):
        if i != ord(' ') and i != ord('\n'):
            r = random.randrange(bit_depth)
            g = random.randrange(bit_depth)
            b = random.randrange(bit_depth)
            char = chr(i)
            colours[char] = (r,g,b)

    return colours


#-------------------------------------------------------------------------------
# Code starts here
#-------------------------------------------------------------------------------
'''
random.seed()
canvas = WizCanvas(78,7)
colours = canvas.generate_colours(4)
canvas.fill_all((0,0,0))

canvas.fill_row(1,(8,8,8))
canvas.fill_column(10,(12,0,0))
print 'this'
#'''












