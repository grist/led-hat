#!/usr/bin/env python
from __future__ import division
__author__ = 'grist'
__doc__ = '''

A script to create a generator that returns x,y coordinates for a line
drawn from x1,y1 to x2,y2.

Preserves direction, so x1,y1 is always the first returned value, and x2, y2
is always the last.

'''
__ver__ = 0.01


#-------------------------------------------------------------------------------  
# Classes
#-------------------------------------------------------------------------------
class Line:
    def __init__(self, x1,y1,x2,y2):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def coords(self):
        ''' Generate the coordinates for points that lie on the line.
        '''

        # variables used often by the algorithm
        delta_x = self.x2 - self.x1
        delta_y = self.y2 - self.y1
    
        # first deal with the 4 straight line cases cos they're easy and don't need
        # the complexity of the main algorithm.
        if delta_x == 0: # North/South
            for y in range(self.y1,self.y2,int((self.y2-self.y1)/abs(self.y2-self.y1))):
                yield(self.x1,y)
            yield(self.x2,self.y2)
        elif delta_y == 0:  # East/West
            for x in range(self.x1,self.x2,int((self.x2-self.x1)/abs(self.x2-self.x1))):
                yield(x,self.y1)
            yield(self.x2,self.y2)
        # now the diagonals
        elif abs(delta_x) > abs(delta_y): # a mostly horizontal line
            m = delta_y/delta_x
            b = self.y1 - m * self.x1
            for x in range(self.x1,self.x2+1,int((self.x2-self.x1)/abs(self.x2-self.x1))):
                y = int((x * m + b) + 0.5)
                yield(x,y)
        else: # a mostly vertical line
            m = delta_x/delta_y
            b = self.x1 - m * self.y1
            for y in range(self.y1,self.y2+1,int((self.y2-self.y1)/abs(self.y2-self.y1))):
                x = int((y * m + b) + 0.5)
                yield(x,y)
    

    

#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------



#-------------------------------------------------------------------------------
# Code starts here
#-------------------------------------------------------------------------------
















