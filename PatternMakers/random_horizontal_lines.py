#!/usr/bin/env python
__author__ = 'grist'
__doc__ = '''

A script to make patterns with randomly coloured lines.

'''
__ver__ = 0.01

import random

#-------------------------------------------------------------------------------  
# Classes
#-------------------------------------------------------------------------------



#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------
def generate_colours(num_colours, bit_depth):
    ''' Generate a random colour pallette with the given bit_depth
    '''
    colours = {}
    for i in range(num_colours):
        r = random.randrange(bit_depth)
        g = random.randrange(bit_depth)
        b = random.randrange(bit_depth)
        char = chr(i)
        colours[char] = (r,g,b)

    return colours

#-------------------------------------------------------------------------------
# Code starts here
#-------------------------------------------------------------------------------

random.seed()















